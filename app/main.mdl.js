(function () {

	'use strict';

	angular.module('main', [
			'ui.router',
			'ui.bootstrap',
			'ngMask',
			'ngCookies',
			'ngRoute',
			'ngDialog',
			'cr.acl',
			'ui-notification',
			'ngFlash',
			'textAngular',
			'flow',
			'angular-loading-bar',
			'nl.sticky',
			'watch',
			'cart',
			'admin',
			'config'
		])
	.config(config)
	.run(run);

	config.$inject = ['$stateProvider', '$urlRouterProvider', 'cfpLoadingBarProvider',
	'NotificationProvider'];

	function config ($stateProvider, $urlRouterProvider, cfpLoadingBarProvider, NotificationProvider) {
		cfpLoadingBarProvider.includeSpinner = false;

        NotificationProvider.setOptions({
            startTop: 25,
            startRight: 25,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'bottom'
        });

        $urlRouterProvider.otherwise(function ($injector) {
        	var $state = $injector.get('$state');
        	var $location = $injector.get('$location');
        	var crAcl = $injector.get('crAcl');

        	var state = "";

        	switch (crAcl.getTole()) {
        		case 'ROLE_ADMIN':
        			state = 'admin.watches';
        			break;
        		default: state = 'main.watch';
        	}

        	if (state) $state.go(state)
        	else $location.path('/');
        });

        $stateProvider
        	.state('main', {
        		url: '/',
        		abstract: true,
        		templateUrl: '../views/main.html',
        		controller: 'CartCtrl as cart',
        		data: {
        			is_granted: ['ROLE_ADMIN']
        		}
        	})
        	.state('blog', {
        		url: '/blog',
        		templateUrl: '../blog.html'
        	})
        	.state('auth', {
        		url: '/login',
        		templateUrl: '../views/auth/login.html',
        		controller: 'AuthCtrl as auth',
        		onEnter: ['AuthService', 'crAcl', function (AuthService, crAcl) {
        			AuthService.clearCredentials();
        			crAcl.setRole();
        		}],
        		data: {
        			is_granted: ['ROLE_GUEST']
        		}
        	});
	}

});